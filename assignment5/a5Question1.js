const createFactor = (x) => { return (n) => x * n; }

var multiplyX = createFactor(10);

console.log(multiplyX(10));
console.log(multiplyX(21));

